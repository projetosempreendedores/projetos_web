package br.com.zenus.testws;

import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class MusicaWs {

	String URL = "http://localhost:8180/zenus/rs";
	String urlEspecifica;

	private DataFactory df;

	@Before
	public void beforeClass() {

		df = new DataFactory();
	}

	@Ignore
	@Test
	public void testIncluirMusica() throws UnirestException {

		urlEspecifica = "/musicas";

		StringBuilder jsonBuilder = new StringBuilder();

		jsonBuilder.append("{");
		jsonBuilder.append("nome: " + df.getStreetName() + ",");
		jsonBuilder.append("artista: " + df.getFirstName() + ",");
		jsonBuilder.append("tom: 1 ,");

		jsonBuilder.append("isAtivo: true,");

		jsonBuilder.append("}");

		JsonNode json = null;
		try {
			json = new JsonNode(jsonBuilder.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpResponse<JsonNode> request = Unirest.post(URL + urlEspecifica).header("accept", "application/json")
				.header("Content-Type", "application/json").body(json).asJson();

		Assert.assertEquals(request.getStatus(), 204);

	}

	@Test
	public void testGetMusicas() throws UnirestException {

		urlEspecifica = "/musicas";

		HttpResponse<JsonNode> request = Unirest.get(URL + urlEspecifica).header("accept", "application/json")
				.header("Content-Type", "application/json").asJson();

		Assert.assertEquals(request.getStatus(), 200);

	}
}
