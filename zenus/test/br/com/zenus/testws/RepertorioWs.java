package br.com.zenus.testws;

import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class RepertorioWs {

	String URL = "http://localhost:8180/zenus/rs";
	String urlEspecifica;

	private DataFactory df;

	@Before
	public void beforeClass() {

		df = new DataFactory();
	}

	@Ignore
	@Test
	public void testIncluirRepertorio() throws UnirestException {

		urlEspecifica = "/repertorios/banda";

		StringBuilder jsonBuilder = new StringBuilder();

		jsonBuilder.append("{");
		jsonBuilder.append("nome: " + df.getBusinessName() + ",");
		jsonBuilder.append("telefone: null,");
		jsonBuilder.append("email: " + df.getEmailAddress() + ",");

		jsonBuilder.append("isAtivo: true,");

		jsonBuilder.append("musicas: [],");

		jsonBuilder.append("musicos: [],");

		jsonBuilder.append("}");

		JsonNode json = null;
		try {
			json = new JsonNode(jsonBuilder.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpResponse<JsonNode> request = Unirest.post(URL + urlEspecifica).header("accept", "application/json")
				.header("Content-Type", "application/json").body(json).asJson();

		Assert.assertEquals(request.getStatus(), 204);

	}

	@Test
	public void testGetBandas() throws UnirestException {

		urlEspecifica = "/repertorios/bandas";

		HttpResponse<JsonNode> request = Unirest.get(URL + urlEspecifica).header("accept", "application/json")
				.header("Content-Type", "application/json").asJson();

		Assert.assertEquals(request.getStatus(), 200);

	}
}
