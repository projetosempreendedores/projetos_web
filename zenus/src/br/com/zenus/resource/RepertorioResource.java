package br.com.zenus.resource;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.zenus.model.Banda;
import br.com.zenus.model.Musica;
import br.com.zenus.service.BandaService;
import br.com.zenus.service.MusicaService;

@Path("/repertorios")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RepertorioResource {

	@EJB
	BandaService bandaService;

	@EJB
	MusicaService musicaService;

	@POST
	@Path("/banda")
	public void salvar(Banda banda) {

		bandaService.salvar(banda);
	}

	@POST
	@Path("/{idBanda}/{idMusica}")
	public void deletarMusica(@PathParam("idBanda") Long idBanda, @PathParam("idMusica") Long idMusica) {

		// Musica musica = (Musica) musicaService.buscar(idMusica);

		Banda banda = (Banda) bandaService.buscar(idBanda);

		List<Musica> musicas = new ArrayList<Musica>();

		for (Musica musica : banda.getMusicas()) {
			musicas.add(musica);
			if (musica.getSeqMusica().longValue() == idMusica) {
				musicas.remove(musica);
			}

		}

		banda.setMusicas(musicas);

		bandaService.salvar(banda);

	}

	@GET
	@Path("/{id}")
	public Response getBanda(@PathParam("id") Long id) {

		Banda banda = (Banda) bandaService.buscar(id);

		if (banda == null) {

			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok(banda).build();

	}

	@GET
	@Path("/bandas")
	public Response getBandas() {

		List<Banda> bandas = bandaService.getBandas();

		if (bandas == null || bandas.isEmpty()) {

			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok(bandas).build();

	}

	@GET
	@Path("/bandas/musicas")
	public Response getBandasMusicas() {

		List<Banda> bandas = bandaService.getBandas();

		if (bandas == null || bandas.isEmpty()) {

			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok(bandas).build();

	}
	
	
	

}
