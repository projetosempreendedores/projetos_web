package br.com.zenus.resource;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.zenus.model.Musica;
import br.com.zenus.model.Musico;
import br.com.zenus.service.MusicoService;

@Path("/musicos")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MusicoResource {

	@EJB
	MusicoService musicoService;

	@POST
	public void salvar(Musico musica) {
		musicoService.salvar(musica);
	}

	@POST
	@Path("/{id}")
	public void deletarMusico(@PathParam("id") Long id) {

		musicoService.deletar(id);

	}

	@GET
	@Path("/{id}")
	public Response getMusico(@PathParam("id") Long id) {

		Musico musica = (Musico) musicoService.buscar(id);

		if (musica == null) {

			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok(musica).build();

	}

	@GET
	public Response getMusicas() {

		List<Musico> musicos = musicoService.getMusicos();

		if (musicos == null || musicos.isEmpty()) {

			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok(musicos).build();

	}

}
