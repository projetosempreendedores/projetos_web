package br.com.zenus.resource;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rs")
public class RestApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {

		return new HashSet<Class<?>>(Arrays.asList(MusicaResource.class, RepertorioResource.class, MusicoResource.class,
				EstudioResource.class));

	}

}
