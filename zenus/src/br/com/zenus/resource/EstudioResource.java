package br.com.zenus.resource;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.zenus.model.Estudio;
import br.com.zenus.service.EstudioService;

@Path("/estudios")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EstudioResource {

	@EJB
	EstudioService estudioService;

	@POST
	public void salvar(Estudio estudio) {
		estudioService.salvar(estudio);
	}

	@POST
	@Path("/{id}")
	public void deletarEstudio(@PathParam("id") Long id) {

		estudioService.deletar(id);

	}

	@GET
	@Path("/{id}")
	public Response getEstudio(@PathParam("id") Long id) {

		Estudio estudio = (Estudio) estudioService.buscar(id);

		if (estudio == null) {

			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok(estudio).build();

	}

	@GET
	public Response getEstudios() {

		List<Estudio> estudios = estudioService.getEstudios();

		if (estudios == null || estudios.isEmpty()) {

			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok(estudios).build();

	}

}
