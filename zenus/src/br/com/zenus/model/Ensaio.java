package br.com.zenus.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ensaio")
public class Ensaio implements Serializable {

	private static final long serialVersionUID = 1L;

	public Ensaio() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "seq_ensaio")
	private Long seqEnsaio;

	@Temporal(value = TemporalType.TIMESTAMP)
	private Date dtEnsaioInicial;

	@Temporal(value = TemporalType.TIMESTAMP)
	private Date dtEnsaioFinal;

	@OneToMany(cascade = CascadeType.ALL, fetch= FetchType.LAZY)
	@JoinTable(name="ensaio_banda", joinColumns=@JoinColumn(name="seq_ensaio"),
	inverseJoinColumns=@JoinColumn(name="seq_banda"))
	private Collection<Banda> bandas;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "repertorio_musica")
	private Collection<Estudio> estudio;

	public Collection<Estudio> getEstudio() {
		return estudio;
	}

	public void setEstudio(Collection<Estudio> estudio) {
		this.estudio = estudio;
	}

	public Collection<Banda> getBandas() {
		return bandas;
	}

	public void setBandas(Collection<Banda> bandas) {
		this.bandas = bandas;
	}

	public Long getSeqRepertorio() {
		return seqEnsaio;
	}

	public Date getDtEnsaioInicial() {
		return dtEnsaioInicial;
	}

	public void setDtEnsaioInicial(Date dtEnsaioInicial) {
		this.dtEnsaioInicial = dtEnsaioInicial;
	}

	public Date getDtEnsaioFinal() {
		return dtEnsaioFinal;
	}

	public void setDtEnsaioFinal(Date dtEnsaioFinal) {
		this.dtEnsaioFinal = dtEnsaioFinal;
	}

	public Long getSeqEnsaio() {
		return seqEnsaio;
	}
}
