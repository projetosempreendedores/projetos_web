package br.com.zenus.model;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Musico
 *
 */
@Entity
@Table(name = "musico")
public class Musico implements Serializable {

	private static final long serialVersionUID = 1L;

	public Musico() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "seq_musico")
	private Long seqMusico;

	@Column(nullable = false)
	private String nome;

	@Column(name = "fone")
	private Long telefone;

	@Column(name = "instrumento")
	private int instrumento;

	@Column(name = "ativo")
	private Boolean isAtivo;
	
	
	public Long getTelefone() {
		return telefone;
	}

	public void setTelefone(Long telefone) {
		this.telefone = telefone;
	}

	public int getInstrumento() {
		return instrumento;
	}

	public void setInstrumento(int instrumento) {
		this.instrumento = instrumento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	

	public Boolean getIsAtivo() {
		return isAtivo;
	}

	public void setIsAtivo(Boolean isAtivo) {
		this.isAtivo = isAtivo;
	}

	public Long getSeqMusico() {
		return seqMusico;
	}

}
