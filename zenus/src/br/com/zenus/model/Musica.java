package br.com.zenus.model;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Entity implementation class for Entity: Musica
 *
 */
@Entity
@Table(name = "musica")
public class Musica implements Serializable {

	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "seq_musica")
	private Long seqMusica;

	@Column(nullable = false)
	private String nome;

	@Column(nullable = false)
	private String artista;

	@Column(name = "tom")
	private String tom;

	@Column
	private String observacao;

	@Column(name = "ativo")
	private Boolean isAtivo;
	
	@Column(name = "ultima_avaliacao")
	private Long ultimaAvaliacao;	
	
	@Column(name = "avaliacao_media")
	private Long avaliacaoMedia;	
	
	

	public Long getUltimaAvaliacao() {
		return ultimaAvaliacao;
	}

	public void setUltimaAvaliacao(Long ultimaAvaliacao) {
		this.ultimaAvaliacao = ultimaAvaliacao;
	}

	public Long getAvaliacaoMedia() {
		return avaliacaoMedia;
	}

	public void setAvaliacaoMedia(Long avaliacaoMedia) {
		this.avaliacaoMedia = avaliacaoMedia;
	}

	

	

	public String getTom() {
		return tom;
	}

	public void setTom(String tom) {
		this.tom = tom;
	}

	public Boolean getIsAtivo() {
		return isAtivo;
	}

	public void setIsAtivo(Boolean isAtivo) {
		this.isAtivo = isAtivo;
	}

	public Long getSeqMusica() {
		return seqMusica;
	}

	public void setSeqMusica(Long seqMusica) {
		this.seqMusica = seqMusica;
	}

	public String getArtista() {
		return artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public Musica() {
		super();
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getObservacao() {
		return this.observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

}
