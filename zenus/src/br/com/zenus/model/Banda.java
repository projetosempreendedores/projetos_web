package br.com.zenus.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Entity implementation class for Entity: Musico
 *
 */
@Entity
@Table(name = "banda")

public class Banda implements Serializable {

	private static final long serialVersionUID = 1L;

	public Banda() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "seq_banda")
	private Long seqBanda;

	@Column(nullable = false, unique = true)
	private String nome;

	@Column(name = "fone")
	private Long telefone;

	@Column(name = "email")
	private String email;

	@Column(name = "ativo")
	private Boolean isAtivo;

	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name = "banda_musica")
	private Collection<Musica> musicas;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name = "banda_musico")
	private Collection<Musico> musicos;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getTelefone() {
		return telefone;
	}

	public void setTelefone(Long telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getIsAtivo() {
		return isAtivo;
	}

	public void setIsAtivo(Boolean isAtivo) {
		this.isAtivo = isAtivo;
	}

	public Collection<Musica> getMusicas() {
		
		if(musicas ==null){
			musicas = new ArrayList<Musica>();
		}
		
		return musicas;
	}

	public void setMusicas(Collection<Musica> musicas) {
		this.musicas = musicas;
	}

	public Collection<Musico> getMusicos() {
		
		if(musicos ==null){
			musicos = new ArrayList<Musico>();
		}
		
		
		return musicos;
	}

	public void setMusicos(Collection<Musico> musicos) {
		this.musicos = musicos;
	}

	public Long getSeqBanda() {
		return seqBanda;
	}

	

	

}
