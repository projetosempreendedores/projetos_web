package br.com.zenus.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.zenus.model.Musico;

public class MusicoDAO extends GenericDAO {

	public MusicoDAO() {

	}

	public MusicoDAO(EntityManager entityManager) {

		this.em = entityManager;
	}

	public void salvar(Musico musico) throws Exception {
		if (musico.getSeqMusico() == null) {
			musico.setIsAtivo(Boolean.TRUE);
			this.em.persist(musico);
		} else {
			if (em.find(Musico.class, musico.getSeqMusico()) == null) {
				throw new Exception("Instru��o SQL nao existe!");
			}
			em.merge(musico);
		}
	}

	public void excluir(Musico musico) {

		try {
			musico.setIsAtivo(Boolean.FALSE);
			em.merge(musico);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	public List<Musico> listar() {

		List<Musico> musicos = (List<Musico>) em.createQuery("select m from Musico m where m.isAtivo=true")
				.getResultList();

		return musicos;
	}

	public Musico getMusico(String nome) {

		Musico musico = (Musico) em.createQuery("select m from Musico m where m.nome='" + nome + "'").getSingleResult();

		return musico;
	}

}
