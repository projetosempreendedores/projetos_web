package br.com.zenus.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.zenus.model.Musica;

public class MusicaDAO extends GenericDAO {

	public MusicaDAO() {

	}

	public MusicaDAO(EntityManager entityManager) {

		this.em = entityManager;
	}

	public void salvar(Musica musica) throws Exception {
		if (musica.getSeqMusica() == null) {
			musica.setIsAtivo(Boolean.TRUE);
			this.em.persist(musica);
		} else {
			if (em.find(Musica.class, musica.getSeqMusica()) == null) {
				throw new Exception("Instru��o SQL nao existe!");
			}
			em.merge(musica);
		}
	}

	public void excluir(Musica musica) {

		try {
			musica.setIsAtivo(Boolean.FALSE);
			em.merge(musica);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	public List<Musica> listar() {

		List<Musica> musicas = (List<Musica>) em.createQuery("select m from Musica m where m.isAtivo=true")
				.getResultList();

		return musicas;
	}
	
	@SuppressWarnings("unchecked")
	public Musica getMusica(String nome) {

		Musica musica = (Musica) em.createQuery("select m from Musica m where m.nome='"+nome+"'").getSingleResult();
				

		return musica;
	}

}
