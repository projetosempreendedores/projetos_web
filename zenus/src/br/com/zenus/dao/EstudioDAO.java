package br.com.zenus.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.zenus.model.Estudio;

public class EstudioDAO extends GenericDAO {

	public EstudioDAO() {

	}

	public EstudioDAO(EntityManager entityManager) {

		this.em = entityManager;
	}

	public void salvar(Estudio estudio) throws Exception {
		if (estudio.getSeqEstudio() == null) {
			estudio.setIsAtivo(Boolean.TRUE);
			this.em.persist(estudio);
		} else {
			if (em.find(Estudio.class, estudio.getSeqEstudio()) == null) {
				throw new Exception("Instru��o SQL nao existe!");
			}
			em.merge(estudio);
		}
	}

	public void excluir(Estudio estudio) {

		try {
			estudio.setIsAtivo(Boolean.FALSE);
			em.merge(estudio);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	public List<Estudio> listar() {

		List<Estudio> estudios = (List<Estudio>) em.createQuery("select m from Estudio m where m.isAtivo=true")
				.getResultList();

		return estudios;
	}

	public Estudio getEstudio(String nome) {

		Estudio estudio = (Estudio) em.createQuery("select m from Estudio m where m.nome='" + nome + "'")
				.getSingleResult();

		return estudio;
	}

}
