package br.com.zenus.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.zenus.model.Musica;
import br.com.zenus.model.Usuario;

public class UsuarioDAO extends GenericDAO {

	private UsuarioDAO() {

	}

	public UsuarioDAO(EntityManager entityManager) {

		this.em = entityManager;
	}

	public void salvar(Usuario usuario) throws Exception {
		if (usuario.getSeqUsuario() == null) {
			this.em.persist(usuario);
		} else {
			if (em.find(Musica.class, usuario.getSeqUsuario()) == null) {
				throw new Exception("Instru��o SQL nao existe!");
			}
			em.merge(usuario);
		}
	}

	public void excluir(Long id) {
		Usuario btpInstruacaoSql = em.find(Usuario.class, id);
		em.remove(btpInstruacaoSql);
	}

	public Usuario consultarPorId(Long id) {

		Usuario usuario = null;

		try {
			usuario = em.find(Usuario.class, id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usuario;

	}

	@SuppressWarnings("unchecked")
	public List<Usuario> listar() {

		List<Usuario> usuarios = (List<Usuario>) em.createQuery("select u from Usuario u").getResultList();

		return usuarios;
	}

}
