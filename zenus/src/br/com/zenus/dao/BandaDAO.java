package br.com.zenus.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.zenus.model.Banda;

public class BandaDAO extends GenericDAO {

	public BandaDAO() {

	}

	public BandaDAO(EntityManager entityManager) {

		this.em = entityManager;
	}

	public void salvar(Banda banda) throws Exception {
		if (banda.getSeqBanda() == null) {
			banda.setIsAtivo(Boolean.TRUE);
			this.em.persist(banda);
		} else {
			if (em.find(Banda.class, banda.getSeqBanda()) == null) {
				throw new Exception("Instru��o SQL nao existe!");
			}
			em.merge(banda);
		}
	}

	public void excluir(Banda banda) {
		
		try {
			banda.setIsAtivo(Boolean.FALSE);
			em.merge(banda);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}

	public Banda consultarPorId(Long id) {

		Banda banda = null;

		try {
			banda = em.find(Banda.class, id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return banda;

	}

	@SuppressWarnings("unchecked")
	public List<Banda> listar() {

		List<Banda> bandas = (List<Banda>) em.createQuery("select b from Banda b where b.isAtivo=true").getResultList();

		return bandas;
	}

}
