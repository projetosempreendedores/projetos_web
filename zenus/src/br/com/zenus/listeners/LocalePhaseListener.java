package br.com.zenus.listeners;

import java.util.Locale;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.servlet.http.Cookie;

import br.com.zenus.util.CookiesHelper;


public class LocalePhaseListener {

	
	private static final long serialVersionUID = -885702509958179279L;

    public void afterPhase(PhaseEvent event) {
        Cookie cookie = CookiesHelper.getCookie("zenus.locale");
        if (cookie != null) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.getViewRoot().setLocale(new Locale(cookie.getValue()));
        }
    }

    public void beforePhase(PhaseEvent event) {
        // NOP
    }

    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
	
}
