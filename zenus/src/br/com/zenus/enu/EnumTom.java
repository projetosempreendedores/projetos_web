package br.com.zenus.enu;

import java.util.ArrayList;
import java.util.List;

public enum EnumTom {

	C(1, "C"), CS(2, "C#"), D(3, "D"), DS(4, "D#"), E(5, "E"), F(6, "F"), FS(7, "F#"), G(8, "G"), GS(9, "G#"), A(10,
			"A"), AS(11, "A#"), B(12, "B");

	private Integer codigo;

	private String descricao;

	private static List<String> lstTonalidades;

	private EnumTom(Integer codigo, String descCodigo) {
		this.codigo = codigo;

		this.descricao = descCodigo;

	}

	public static List<String> getLstTonalidades() {
		if (lstTonalidades == null) {

			lstTonalidades = new ArrayList<String>();

			for (EnumTom enumTom : EnumTom.values()) {
				lstTonalidades.add(enumTom.getDescricao());
			}
		}

		return lstTonalidades;
	}

	public static String getDescricao(int codigo) {

		for (EnumTom enumTom : EnumTom.values()) {
			if (codigo == enumTom.getCodigo()) {
				return enumTom.getDescricao();
			}

		}
		return "Ops, nenhum tom encontrado!!!";
	}

	public static Integer getCodigo(String descricao) {

		for (EnumTom enumTom : EnumTom.values()) {
			if (enumTom.getDescricao().equals(descricao.trim())) {
				return enumTom.getCodigo();
			}

		}
		return 0;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

}
