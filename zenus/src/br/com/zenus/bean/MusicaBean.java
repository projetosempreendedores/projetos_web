package br.com.zenus.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import br.com.zenus.enu.EnumTom;
import br.com.zenus.model.Musica;

@ManagedBean
@ViewScoped
public class MusicaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7665793729629784572L;

	@ManagedProperty("#{musicaViewService}")
	private MusicaViewService musicaViewService;

	@PostConstruct
	public void init() {

		musicas = musicaViewService.createMusicas();

	}
	//Nova API para buscar dados de m�sica 
	
	//http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=a3311bb7ccd518328a4c6748a52197ab&artist=silvershair&track=miss%20you%20love&format=json
	
	private Musica musica;

	public void novo() {

		musica = new Musica();

	}

	private List<Musica> musicas;

	private List<Musica> filteredMusicas;

	public List<String> getFilteredTom() {

		return EnumTom.getLstTonalidades();
	}

	public List<Musica> getFilteredMusicas() {
		return filteredMusicas;
	}

	public void setFilteredMusicas(List<Musica> filteredMusicas) {
		this.filteredMusicas = filteredMusicas;
	}

	

	public List<Musica> getMusicas() {
		
		if(musicas==null){
			
			musicas = new ArrayList<Musica>();
			musicas = musicaViewService.createMusicas();
		}

		return musicas;
	}

	public void setMusicas(List<Musica> musicas) {
		this.musicas = musicas;
	}

	public MusicaViewService getMusicaViewService() {
		return musicaViewService;
	}

	public void setMusicaViewService(MusicaViewService musicaViewService) {
		this.musicaViewService = musicaViewService;
	}

	public Musica getMusica() {
		return musica;
	}

	public void setMusica(Musica musica) {
		this.musica = musica;
	}

	public void salvar(ActionEvent actionEvent) {

		try {
			musicaViewService.saveMusica(musica);

			addMessage("Item salvo com sucesso!!");
			
			
		} catch (Exception e) {
			addMessageException("Ops, Erro ao salvar m�sica!!!");
		}finally{
			
			musicas = musicaViewService.createMusicas();
		}
	}

	public void excluir(ActionEvent actionEvent) {

		try {
			musicaViewService.deletar(musica);

			addMessage("Item exclu�do com sucesso!!");
			
			
		} catch (Exception e) {
			addMessageException("Ops, Erro ao excluir m�sica!!!");
		}finally {
			musicas = musicaViewService.createMusicas();
		}
	}

	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void addMessageException(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

}
