package br.com.zenus.bean;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import br.com.zenus.model.Banda;
import br.com.zenus.service.BandaService;

@ManagedBean(name = "bandaViewService")
@ApplicationScoped
public class BandaViewService {

	@EJB
	BandaService bandaService;

	public List<Banda> createBandas() {

		return bandaService.getBandas();
	}

	public void saveBanda(Banda banda) {

		bandaService.salvar(banda);
	}

	public void deletar(Banda banda) {

		bandaService.deletar(banda);
	}

}
