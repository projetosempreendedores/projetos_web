package br.com.zenus.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.model.DualListModel;

import br.com.zenus.model.Banda;
import br.com.zenus.model.Musica;

@ManagedBean
@ViewScoped
public class BandaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7665793729629784572L;

	private List<Banda> bandas;

	private List<Banda> filteredBandas;

	private List<Musica> musicas;

	private DualListModel<String> nomeMusicas;

	private Banda banda;

	@ManagedProperty("#{bandaViewService}")
	private BandaViewService bandaViewService;

	@ManagedProperty("#{musicaViewService}")
	private MusicaViewService musicaViewService;

	@PostConstruct
	public void init() {

		bandas = bandaViewService.createBandas();

	}

	public void novo() {

		banda = new Banda();
	}

	public void salvar(ActionEvent actionEvent) {

		try {

			if (nomeMusicas != null && !nomeMusicas.getTarget().isEmpty()) {

				banda.getMusicas().clear();

				List<Musica> musicas = new ArrayList<Musica>();

				for (String nome : nomeMusicas.getTarget()) {

					Musica musica = musicaViewService.buscar(nome);

					musicas.add(musica);
				}

				banda.setMusicas(musicas);

			}

			bandaViewService.saveBanda(banda);

			addMessage("Item salvo com sucesso!!");

		} catch (Exception e) {
			addMessageException("Ops, Erro ao salvar banda!!!");
		} finally {

			bandas = bandaViewService.createBandas();
		}
	}

	public void excluir(ActionEvent actionEvent) {

		try {

			bandaViewService.deletar(banda);

			addMessage("Item exclu�do com sucesso!!");

		} catch (Exception e) {
			addMessageException("Ops, Erro ao excluir banda!!!");
		} finally {
			bandas = bandaViewService.createBandas();
		}
	}

	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void addMessageException(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public List<Banda> getBandas() {

		if (bandas == null) {

			bandas = new ArrayList<Banda>();
		}

		return bandas;
	}

	

	public List<String> getFilterBanda() {

		List<String> filtro = new ArrayList<String>();

		for (Banda banda : bandas) {

			filtro.add(banda.getNome());

		}

		return filtro;
	}

	public DualListModel<String> getMusicas() {

		List<String> nomes = new ArrayList<String>();
		List<String> nomesSelecionados = new ArrayList<String>();

		if (musicas == null) {

			musicas = new ArrayList<Musica>();

			musicas = musicaViewService.createMusicas();

			if (banda != null) {

				for (Musica musica : banda.getMusicas()) {

					nomesSelecionados.add(musica.getNome());
				}
			}

			for (Musica musica : musicas) {

				nomes.add(musica.getNome());

			}

			nomeMusicas = new DualListModel<String>(nomes, nomesSelecionados);
			
		
		}

		return nomeMusicas;
	}

	public void setMusicas(DualListModel<String> nomeMusicas) {
		this.nomeMusicas = nomeMusicas;
	}

	public void setBandas(List<Banda> bandas) {
		this.bandas = bandas;
	}

	public List<Banda> getFilteredBandas() {
		return filteredBandas;
	}

	public void setFilteredBandas(List<Banda> filteredBandas) {
		this.filteredBandas = filteredBandas;
	}

	public Banda getBanda() {
		return banda;
	}

	public void setBanda(Banda banda) {
		this.banda = banda;
	}

	public BandaViewService getBandaViewService() {
		return bandaViewService;
	}

	public MusicaViewService getMusicaViewService() {
		return musicaViewService;
	}

	public void setMusicaViewService(MusicaViewService musicaViewService) {
		this.musicaViewService = musicaViewService;
	}

	public void setMusicas(List<Musica> musicas) {
		this.musicas = musicas;
	}

	public void setBandaViewService(BandaViewService bandaViewService) {
		this.bandaViewService = bandaViewService;
	}

	public List<Musica> getMusica() {
		return musicas;
	}

	public void setMusica(List<Musica> musica) {
		this.musicas = musica;
	}

}
