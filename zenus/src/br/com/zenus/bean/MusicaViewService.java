package br.com.zenus.bean;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import br.com.zenus.model.Musica;
import br.com.zenus.service.MusicaService;

@ManagedBean(name = "musicaViewService")
@ApplicationScoped
public class MusicaViewService {

	@EJB
	MusicaService musicaService;

	public List<Musica> createMusicas() {

		return musicaService.getMusicas();
	}

	public Musica buscar(String nome) {

		return musicaService.buscar(nome);
	}

	
	public void saveMusica(Musica musica) {

		musicaService.salvar(musica);
	}

	public void deletar(Musica musica) {

		musicaService.deletar(musica);
	}

}
