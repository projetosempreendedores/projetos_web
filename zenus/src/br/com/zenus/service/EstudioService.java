package br.com.zenus.service;

import java.util.List;

import javax.ejb.Remote;

import br.com.zenus.model.Estudio;

@Remote
public interface EstudioService extends GenericService {

	public List<Estudio> getEstudios();

	public Estudio buscar(String nome);

}
