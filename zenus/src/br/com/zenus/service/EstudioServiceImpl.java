package br.com.zenus.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.zenus.dao.EstudioDAO;
import br.com.zenus.model.Estudio;

@Stateless(name = "EstudioService")
public class EstudioServiceImpl implements EstudioService {

	@PersistenceContext(name = "zenus")
	private EntityManager em;

	private EstudioDAO estudioDAO;

	public void salvar(Object object) {

		estudioDAO = new EstudioDAO(em);

		try {
			estudioDAO.salvar((Estudio) object);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Estudio> getEstudios() {

		estudioDAO = new EstudioDAO(em);

		return estudioDAO.listar();

	}

	public Object buscar(Long id) {

		estudioDAO = new EstudioDAO(em);

		return em.find(Estudio.class, id);

	}

	public void deletar(Object object) {

		estudioDAO = new EstudioDAO(em);

		try {
			estudioDAO.excluir((Estudio) object);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Estudio buscar(String nome) {

		estudioDAO = new EstudioDAO(em);

		return estudioDAO.getEstudio(nome);
	}

}
