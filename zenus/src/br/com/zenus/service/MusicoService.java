package br.com.zenus.service;

import java.util.List;

import javax.ejb.Remote;

import br.com.zenus.model.Musico;

@Remote
public interface MusicoService extends GenericService {

	public List<Musico> getMusicos();

	public Musico buscar(String nome);

}
