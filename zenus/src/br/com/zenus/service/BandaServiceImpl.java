package br.com.zenus.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.zenus.dao.BandaDAO;
import br.com.zenus.model.Banda;

@Stateless(name = "BandaService")
public class BandaServiceImpl implements BandaService {

	@PersistenceContext(name = "zenus")
	private EntityManager em;

	private BandaDAO bandaDAO;

	public void salvar(Object object) {

		bandaDAO = new BandaDAO(em);

		try {
			bandaDAO.salvar((Banda) object);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Banda> getBandas() {

		bandaDAO = new BandaDAO(em);

		return bandaDAO.listar();

	}

	public Object buscar(Long id) {

		bandaDAO = new BandaDAO(em);

		return bandaDAO.consultarPorId(id);

	}

	public void deletar(Object object) {

		bandaDAO = new BandaDAO(em);

		try {
			bandaDAO.excluir((Banda) object);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
