package br.com.zenus.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.zenus.dao.MusicoDAO;
import br.com.zenus.model.Musica;
import br.com.zenus.model.Musico;

@Stateless(name = "MusicoService")
public class MusicoServiceImpl implements MusicoService {

	@PersistenceContext(name = "zenus")
	private EntityManager em;

	private MusicoDAO musicoDAO;

	public void salvar(Object object) {

		musicoDAO = new MusicoDAO(em);

		try {
			musicoDAO.salvar((Musico) object);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Musico> getMusicos() {

		musicoDAO = new MusicoDAO(em);

		return musicoDAO.listar();

	}

	public Object buscar(Long id) {

		musicoDAO = new MusicoDAO(em);

		return em.find(Musica.class, id);

	}

	public void deletar(Object object) {

		musicoDAO = new MusicoDAO(em);

		try {
			musicoDAO.excluir((Musico) object);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Musico buscar(String nome) {

		musicoDAO = new MusicoDAO(em);

		return musicoDAO.getMusico(nome);
	}

}
