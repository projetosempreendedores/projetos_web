package br.com.zenus.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.zenus.dao.MusicaDAO;
import br.com.zenus.model.Musica;

@Stateless(name = "MusicaService")
public class MusicaServiceImpl implements MusicaService {

	@PersistenceContext(name = "zenus")
	private EntityManager em;

	private MusicaDAO musicaDAO;

	public void salvar(Object object) {

		musicaDAO = new MusicaDAO(em);

		try {
			musicaDAO.salvar((Musica) object);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Musica> getMusicas() {

		musicaDAO = new MusicaDAO(em);

		return musicaDAO.listar();

	}

	public Object buscar(Long id) {

		musicaDAO = new MusicaDAO(em);

		return em.find(Musica.class, id);

	}

	public void deletar(Object object) {

		musicaDAO = new MusicaDAO(em);

		try {
			musicaDAO.excluir((Musica) object);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Musica buscar(String nome) {

		musicaDAO = new MusicaDAO(em);

		return musicaDAO.getMusica(nome);
	}

}
