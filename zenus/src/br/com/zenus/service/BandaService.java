package br.com.zenus.service;

import java.util.List;

import javax.ejb.Remote;

import br.com.zenus.model.Banda;

@Remote
public interface BandaService extends GenericService {

	public List<Banda> getBandas();

	
}
