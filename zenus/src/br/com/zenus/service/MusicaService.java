package br.com.zenus.service;

import java.util.List;

import javax.ejb.Remote;

import br.com.zenus.model.Musica;

@Remote
public interface MusicaService extends GenericService {

	public List<Musica> getMusicas();
	
	public Musica buscar (String nome);

	
}
