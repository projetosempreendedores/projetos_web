package br.com.chat.service;

import java.util.List;

import javax.ejb.Remote;

@Remote
public interface UsuarioService extends GenericService {

	public List<Usuario> getMusicas();
	
	public Usuario buscar (String nome);

	
}
