package teste;

import javax.persistence.EntityManager;

public class ClienteDAO extends DAO{
	
	
	public void salvar(Cliente cliente){
		
		EntityManager em = getEntityManager();
		
		try {
			em.getTransaction().begin();
			em.persist(cliente);
			em.getTransaction().commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			em.getTransaction().rollback();
		}
		
	}

}
